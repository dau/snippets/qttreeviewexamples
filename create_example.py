import h5py
import numpy


with h5py.File("test.h5", "w") as nxroot:
    nxroot.attrs["NX_class"] = "NXroot"
    nxroot.attrs["default"] = "2.1"

    nxentry = nxroot.create_group("1.1")
    nxentry.attrs["NX_class"] = "NXentry"
    nxentry.attrs["default"] = "data"
    nxentry["title"] = "1D scan"

    nxdata = nxentry.create_group("data")
    nxdata.attrs["NX_class"] = "NXdata"
    nxdata.attrs["axes"] = ["x"]
    nxdata.attrs["signal"] = "y"
    nxdata["title"] = "1D scan"
    x = numpy.linspace(0, 4 * numpy.pi, 400)
    y = numpy.sin(x)
    nxdata["x"] = x
    nxdata["y"] = y

    nxentry = nxroot.create_group("2.1")
    nxentry.attrs["NX_class"] = "NXentry"
    nxentry.attrs["default"] = "data"
    nxentry["title"] = "2D scan"

    nxdata = nxentry.create_group("data")
    nxdata.attrs["NX_class"] = "NXdata"
    nxdata.attrs["axes"] = ["x", "y"]
    nxdata.attrs["signal"] = "z"
    nxdata["title"] = "2D scan"
    x = numpy.linspace(0, 4 * numpy.pi, 400)
    y = numpy.linspace(0, 2 * numpy.pi, 400)
    xx, yy = numpy.meshgrid(x, y)
    z = numpy.sin((xx**2 + yy**2) ** 0.5)
    nxdata["x"] = x
    nxdata["y"] = y
    nxdata["z"] = z
