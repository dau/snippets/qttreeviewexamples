import os
import sys
from glob import glob
from contextlib import contextmanager
from typing import Optional, Union, Sequence, Generator, List, Iterator

import h5py

from AnyQt import QtGui
from AnyQt import QtCore
from AnyQt import QtWidgets
from AnyQt.QtCore import Qt

from silx.gui import icons
from silx.io import h5py_utils
from silx.gui.data.DataViewerFrame import DataViewerFrame


class HDF5DataViewer(QtWidgets.QWidget):
    """HDF5 tree browser with plotting similar to :code:`silx view`"""

    def __init__(self, parent=None):
        super().__init__(parent)
        layout = QtWidgets.QVBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)

        toolbar = QtWidgets.QToolBar(self)
        layout.addWidget(toolbar)
        toolbar.setIconSize(QtCore.QSize(16, 16))

        model = HDF5TreeModel()

        action = QtWidgets.QAction(toolbar)
        action.setIcon(icons.getQIcon("view-refresh"))
        action.setText("Refresh")
        action.setToolTip("Refresh all files")
        action.triggered.connect(model.reload_all_files)
        action.setShortcuts(
            [
                QtGui.QKeySequence(Qt.Key_F5),
                QtGui.QKeySequence(Qt.CTRL | Qt.Key_R),
            ]
        )
        toolbar.addAction(action)

        splitter = QtWidgets.QSplitter(self)
        layout.addWidget(splitter)
        treeView = QtWidgets.QTreeView(self)
        splitter.addWidget(treeView)
        dataViewerFrame = DataViewerFrame()
        splitter.addWidget(dataViewerFrame)

        splitter.setSizes([1000, 1000])

        treeView.setModel(model)
        treeView.clicked.connect(self.onItemClicked)

        treeView.setContextMenuPolicy(Qt.CustomContextMenu)
        treeView.customContextMenuRequested.connect(self.onContextMenuRequested)
        treeView.setColumnWidth(0, 150)

        self.dataViewerFrame = dataViewerFrame
        self.treeView = treeView
        self.model = model

    def onItemClicked(self, index):
        item = index.internalPointer()
        self.dataViewerFrame.setData(item.get_plot_data())

    def onContextMenuRequested(self, point):
        index = self.treeView.indexAt(point)
        if index.isValid():
            item = index.internalPointer()
            menu = item.contextMenu(self.model)
            if menu:
                menu.exec_(self.treeView.viewport().mapToGlobal(point))


class HDF5TreeModel(QtCore.QAbstractItemModel):
    """HDF5 tree model for :code:`QTreeView`"""

    def __init__(self, parent=None):
        super().__init__(parent)
        self._rootItem = HDF5TreeItem()
        self._file_open_options = {"mode": "r", "locking": False}

    def rowCount(self, parent=QtCore.QModelIndex()) -> int:
        if not parent.isValid():
            return self._rootItem.childCount()
        parentItem = parent.internalPointer()
        return parentItem.childCount()

    def columnCount(self, parent=QtCore.QModelIndex()) -> int:
        return 3

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return None
        if role == Qt.DisplayRole:
            item = index.internalPointer()
            return item.data(index.column())
        if role == Qt.DecorationRole:
            item = index.internalPointer()
            return item.icon(index.column())
        if role == Qt.ToolTipRole:
            item = index.internalPointer()
            return item.tooltip(index.column())

    def headerData(
        self, section, orientation, role=Qt.DisplayRole
    ) -> Optional[List[str]]:
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            headers = ["Name", "Type", "Shape"]
            if section < len(headers):
                return headers[section]
        return None

    def index(self, row, column, parent=QtCore.QModelIndex()) -> QtCore.QModelIndex:
        if not self.hasIndex(row, column, parent):
            return QtCore.QModelIndex()
        if not parent.isValid():
            parentItem = self._rootItem
        else:
            parentItem = parent.internalPointer()
        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        return QtCore.QModelIndex()

    def parent(self, index: QtCore.QModelIndex) -> QtCore.QModelIndex:
        if not index.isValid():
            return QtCore.QModelIndex()
        childItem = index.internalPointer()
        parentItem = childItem.parent
        if parentItem == self._rootItem:
            return QtCore.QModelIndex()
        return self.createIndex(parentItem.row(), 0, parentItem)

    def add_files(self, file_paths: Sequence[str]) -> None:
        self.beginResetModel()
        for file_path_pattern in file_paths:
            for file_path in glob(file_path_pattern):
                file_item = HDF5TreeItem(
                    file_path=file_path,
                    parent=self._rootItem,
                    file_open_options=self._file_open_options,
                )
                self._rootItem.addChild(file_item)
        self.endResetModel()

    def remove_file(self, file_path: str) -> None:
        self.beginResetModel()
        for idx, child in enumerate(self._rootItem.iter_children()):
            if child.file_path == file_path:
                self.beginRemoveRows(QtCore.QModelIndex(), idx, idx)
                self._rootItem.removeChild(idx)
                self.endRemoveRows()
                break
        self.endResetModel()

    def reload_all_files(self) -> None:
        self.beginResetModel()
        for child in self._rootItem.iter_children():
            child.invalidate()
        self.endResetModel()


class HDF5TreeItem:
    """Item for :code:`HDF5TreeModel`"""

    def __init__(
        self,
        data_path: Optional[str] = None,
        file_path: Optional[str] = None,
        file_open_options: Optional[dict] = None,
        h5_item: Optional[Union[h5py.Group, h5py.Dataset, Exception]] = None,
        parent: Optional["HDF5TreeItem"] = None,
    ):
        """
        :param data_path: full HDF5 item name
        :param file_path: HDF5 file name
        :param h5_item: HDF5 object from which to extract item information
        :param parent: parent in the Qt tree
        """
        if file_path and not data_path:
            data_path = "/"
        if file_path and file_open_options is None:
            file_open_options = dict()

        self._data_path = data_path
        self._file_path = file_path
        self._file_open_options = file_open_options
        self._parent = parent
        self._children = []
        self._is_loaded = False

        # Not an HDF5 item
        self._is_h5item = False
        self._is_h5group = None
        self._h5item_ndim = None
        self._h5item_shape = None
        self._h5item_type = None
        if isinstance(h5_item, h5py.Dataset):
            # HDF5 dataset
            self._is_h5item = True
            self._is_h5group = False
            self._h5item_ndim = h5_item.ndim
            self._h5item_shape = h5_item.shape
            self._h5item_type = h5_item.dtype
        elif isinstance(h5_item, h5py.Group):
            # HDF5 group
            self._is_h5item = True
            self._is_h5group = True
            self._h5item_type = h5_item.attrs.get("NX_class", "")
        elif h5_item is not None:
            # HDF5 item that cannot be accessed
            self._is_h5item = True
        elif file_path:
            # HDF5 root group
            self._is_h5item = True
            self._is_h5group = True
            self._h5item_type = ""

    def __str__(self) -> str:
        if not self.is_hdf5_item:
            return super().__str__()
        if self.is_hdf5_dataset:
            itype = "dataset"
        elif self.is_hdf5_group:
            itype = "group"
        else:
            itype = "broken"
        return f"{self._file_path}::{self._data_path} ({itype})"

    @property
    def parent(self) -> Optional["HDF5TreeItem"]:
        return self._parent

    @property
    def file_path(self) -> Optional[str]:
        return self._file_path

    @property
    def is_hdf5_item(self) -> bool:
        return self._is_h5item

    @property
    def is_hdf5_group(self) -> bool:
        return self.is_hdf5_item and self._is_h5group is True

    @property
    def is_hdf5_root(self) -> bool:
        return self.is_hdf5_group and self._data_path == "/"

    @property
    def is_hdf5_dataset(self) -> bool:
        return self.is_hdf5_item and self._is_h5group is False

    @contextmanager
    def open(self) -> Generator[Optional[Union[h5py.Dataset, h5py.Group]], None, None]:
        if self.is_hdf5_item:
            file = None
            try:
                try:
                    file = h5py_utils.File(self._file_path, **self._file_open_options)
                    h5item = file[self._data_path]
                except Exception:
                    self._is_h5group = None
                    h5item = None
                yield h5item
            finally:
                if file is not None:
                    file.close()
        else:
            yield None

    def get_plot_data(self):
        with self.open() as h5item:
            if isinstance(h5item, h5py.Dataset):
                return h5item[()]
            if isinstance(h5item, h5py.Group):
                default = h5item
                while True:
                    default_attr = default.attrs.get("default", None)
                    if default_attr:
                        default = default[default_attr]
                    else:
                        break
                is_nxdata = default.attrs.get("NX_class", None) == "NXdata"
                if is_nxdata:
                    # TODO: cause silx to select silx.gui.data.DataViews._NXdataView
                    return {k: v[()] for k, v in default.items()}
                # TODO: cause silx to select silx.gui.data.DataViews._Hdf5View
                return dict(h5item.attrs)

    def invalidate(self) -> None:
        """Item data will be reloaded the first time when needed"""
        self._is_loaded = False

    def _load_children(self) -> None:
        if self._is_loaded:
            return
        self._children.clear()

        if not self.is_hdf5_group:
            self._is_loaded = True
            return

        if self.is_hdf5_root:
            parent_name = ""
        else:
            parent_name = self._data_path

        with self.open() as h5_parent_item:
            for key in h5_parent_item or []:
                try:
                    h5_item = h5_parent_item[key]
                except Exception as e:
                    h5_item = e
                self._children.append(
                    HDF5TreeItem(
                        data_path=f"{parent_name}/{key}",
                        file_path=self._file_path,
                        file_open_options=self._file_open_options,
                        h5_item=h5_item,
                        parent=self,
                    )
                )

        self._is_loaded = True

    def child(self, row: int) -> "HDF5TreeItem":
        self._load_children()
        return self._children[row]

    def iter_children(self) -> Iterator["HDF5TreeItem"]:
        yield from self._children

    def childCount(self) -> int:
        self._load_children()
        return len(self._children)

    def addChild(self, child: "HDF5TreeItem") -> None:
        self._load_children()
        self._children.append(child)

    def removeChild(self, row: int) -> None:
        del self._children[row]

    def row(self) -> int:
        if self._parent:
            return self._parent._children.index(self)
        return 0

    def columnCount(self) -> int:
        return 4

    def data(self, column: int) -> str:
        if column == 0:
            if self.is_hdf5_root:
                return os.path.basename(self._file_path)
            return self._data_path.split("/")[-1]
        elif column == 1 and self.is_hdf5_item:
            return str(self._h5item_type)
        elif column == 2 and self.is_hdf5_dataset:
            return str(self._h5item_shape)
        return ""

    def icon(self, column: int) -> Optional[QtGui.QIcon]:
        if column != 0:
            return

        if self.is_hdf5_root:
            return self.get_icon("SP_FileIcon")

        if self.is_hdf5_group:
            if self._h5item_type.startswith("NX"):
                return self.get_icon("SP_DirIcon", "layer-nx")
            return self.get_icon("SP_DirIcon")

        if self.is_hdf5_dataset:
            if self._h5item_ndim == 0:
                return self.get_icon("item-0dim")
            if self._h5item_ndim == 1:
                return self.get_icon("item-1dim")
            if self._h5item_ndim == 2:
                return self.get_icon("item-2dim")
            if self._h5item_ndim == 3:
                return self.get_icon("item-3dim")
            return self.get_icon("item-ndim")

        if self.is_hdf5_item:
            return self.get_icon("SP_MessageBoxCritical")

    _ICONS = dict()

    @classmethod
    def get_icon(cls, name: str, foreground_name: Optional[str] = None) -> QtGui.QIcon:
        key = name, foreground_name

        icon = cls._ICONS.get(key)
        if icon is not None:
            return icon
        try:
            icon = icons.getQIcon(name)
        except Exception:
            style = QtWidgets.QApplication.style()
            icon = style.standardIcon(getattr(QtWidgets.QStyle, name))

        if foreground_name:
            icon = cls._compound_icon(icon, cls.get_icon(foreground_name))

        cls._ICONS[key] = icon
        return icon

    @classmethod
    def _compound_icon(cls, backgroundIcon, foregroundIcon) -> QtGui.QIcon:
        icon = QtGui.QIcon()

        sizes = backgroundIcon.availableSizes()
        sizes = sorted(sizes, key=lambda s: s.height())
        sizes = filter(lambda s: s.height() < 100, sizes)
        sizes = list(sizes)
        if len(sizes) > 0:
            baseSize = sizes[-1]
        else:
            baseSize = QtCore.QSize(32, 32)

        modes = [QtGui.QIcon.Normal, QtGui.QIcon.Disabled]
        for mode in modes:
            pixmap = QtGui.QPixmap(baseSize)
            pixmap.fill(Qt.transparent)
            painter = QtGui.QPainter(pixmap)
            painter.drawPixmap(0, 0, backgroundIcon.pixmap(baseSize, mode=mode))
            painter.drawPixmap(0, 0, foregroundIcon.pixmap(baseSize, mode=mode))
            painter.end()
            icon.addPixmap(pixmap, mode=mode)

        return icon

    def tooltip(self, column: int) -> str:
        return str(self)

    def contextMenu(self, model) -> Optional[QtWidgets.QMenu]:
        if not self.is_hdf5_item:
            return
        menu = QtWidgets.QMenu()
        action_remove = menu.addAction("Remove File")
        action_remove.triggered.connect(lambda: self.removeActionTriggered(model))
        return menu

    def removeActionTriggered(self, model):
        model.remove_file(self._file_path)


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("HDF5 File Viewer")
        self.dataviewer = HDF5DataViewer(self)
        self.setCentralWidget(self.dataviewer)
        self.createActions()
        self.createMenus()
        self.resize(900, 600)

    def createActions(self):
        self.loadFileAction = QtWidgets.QAction("Load HDF5 File(s)", self)
        self.loadFileAction.triggered.connect(self.loadHDF5Files)

    def createMenus(self):
        self.fileMenu = self.menuBar().addMenu("&File")
        self.fileMenu.addAction(self.loadFileAction)

    def loadHDF5Files(self):
        options = QtWidgets.QFileDialog.Options()
        file_paths, _ = QtWidgets.QFileDialog.getOpenFileNames(
            self,
            "Open HDF5 File(s)",
            "",
            "HDF5 Files (*.h5 *.hdf5);;All Files (*)",
            options=options,
        )
        if file_paths:
            self.dataviewer.model.add_files(file_paths)


def main(argv=None) -> Optional[int]:
    import argparse

    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser(
        description="Load HDF5 files and display their structure."
    )
    parser.add_argument("files", nargs="+", help="List of HDF5 files to load.")
    args = parser.parse_args(args=argv[1:])

    app = QtWidgets.QApplication(argv)
    window = MainWindow()
    window.dataviewer.model.add_files(args.files)
    window.show()
    return app.exec_()


if __name__ == "__main__":
    sys.exit(main())
